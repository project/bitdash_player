

bitdash_player 7.x-1.x-dev
-----------------------------

* #2792115 by SilvanWakker: Non-static function called statically: copyDirectory()
* Add CHANGELOG.md

bitdash_player 7.x-1.0-alpha2
-----------------------------

* Update the README file with a description

bitdash_player 7.x-1.0-alpha1
-----------------------------
